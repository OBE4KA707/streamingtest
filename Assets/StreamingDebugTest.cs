﻿using UnityEngine;

public class StreamingDebugTest : MonoBehaviour
{    
    void OnGUI()
    {
        string line = "The amount of memory currently being used by textures: " + Texture.currentTextureMemory / 1024 / 1024 + "\n" +
                      "This amount of texture memory would be used before the texture streaming budget is applied: " + Texture.desiredTextureMemory / 1024 / 1024 + "\n" +
                      "The amount of memory used by textures after the mipmap streaming and budget are applied and loading is complete: " + Texture.targetTextureMemory / 1024 / 1024 + "\n" +
                      "The total amount of memory that would be used by all textures at mipmap level 0: " + Texture.totalTextureMemory / 1024 / 1024; 
        GUI.Label(new Rect(10, 10, 2000, 1000), line);
    }
}
